#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

//You need to install and include both wigxjpf and fastwigxj.
//WIGXJPF evaluates Wigner 3j, 6j and 9j symbols accurately using prime factorisation and multi-word integer arithmetic. Webpage http://fy.chalmers.se/subatom/wigxjpf/
//FASTWIGXJ evaluates Wigner 3j, 6j and 9j symbols quickly by lookup in precalculated tables. Webpage http://fy.chalmers.se/subatom/fastwigxj/
#include "wigxjpf.h"
#include "fastwigxj.h"

// -------   UTILITIES     ------- //

// ------- Linear Algebra  ------- //
//We use LAPACK (Linear Algebra PACKage) to perform the product of matrices efficiently. We encapsulate dgemm_ into our own functions to make the product (contraction of row/column indices) of two tensors. For reference we recall the arguments and functioning of dgemm_
extern void dcopy_(int *n, double *dx, int *incx, double *dy, int *incy);
extern void dsyev_( char* jobz, char* uplo, int* n, double* a, int* lda, double* w, double* work, int* lwork, int* info );
extern void dgemm_(char *TRANSA, char *TRANSB, int *M, int *N, int *K, double *ALPHA, double *A, int *LDA, double *B, int *LDB, double *BETA, double *C, int *LDC);

// DGEMM  performs one of the matrix-matrix operations
//    C := ALPHA*opA( A )*opB( B ) + BETA*C,
// where  opA( X ) is opA( X ) = X if TRANSA=N or opA( X ) = X**T if TRANSA=T. The analog for opB and TRANSB.
// ALPHA and BETA are scalars, and A, B and C are matrices, with opA( A ) is an m by k matrix,  opB( B ) is a k by n matrix and C an m by n matrix.

// mmult compute C=A**T*B**T where A has nrA rows, ncA columns and B has nrB rows, ncB columns while C has the size sizeC (rows times columns)
void mmult(double* A, double* B, double* C, int nrA, int ncA, int nrB, int ncB, int sizeC) {
	if (ncA!=nrB) {
		printf("Incompatible dimensions!\n");
		return;
	}
	if (sizeC<nrA*ncB) {
		printf("C too small to contain the result\n");
		return;
	}
	char TRANSA = 'T', TRANSB = 'T';
	int M = nrA, N = ncB, K =ncA;
	double ALPHA = 1.0;
        int LDA = K, LDB = N, LDC = M;
        double BETA = 0.0;
        double* temp = calloc(M*N,sizeof(double));

	dgemm_(&TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, temp, &LDC);
	for (int i = 0; i<M; i++) {
		for (int j = 0; j<N; j++) {
			C[N*i+j]=temp[M*j+i];
		}
	}
	free(temp);
}

// tmult compute C=A**T*B where A has nrA rows, ncA columns and B has nrB rows, ncB columns while C has the size sizeC (rows times columns)
void tmmult(double* A, double* B, double* C, int nrA, int ncA, int nrB, int ncB, int sizeC) {
	if (ncA!=nrB) {
		printf("Incompatible dimensions!\n");
		return;
	}
	if (sizeC<nrA*ncB) {
		printf("C too small to contain the result\n");
		return;
	}
	char TRANSA = 'T', TRANSB = 'N';
	int M = nrA, N = ncB, K =ncA;
	double ALPHA = 1.0;
        int LDA = K, LDB = K, LDC = M;
        double BETA = 0.0;
        double* temp = calloc(M*N,sizeof(double));

	dgemm_(&TRANSA, &TRANSB, &M, &N, &K, &ALPHA, A, &LDA, B, &LDB, &BETA, temp, &LDC);
	for (int i = 0; i<M; i++) {
		for (int j = 0; j<N; j++) {
			C[N*i+j]=temp[M*j+i];
		}
	}
	free(temp);
}

//Compute the trace of a tensor m of dimension dim
double trace(double* m, int dim) {
	double tr = 0.0;
	for (int i=0; i<dim; i++) {
		tr += m[dim*i+i];
	}
	return tr;
}

//Diagonalize the matrix of size matrix_size and compute its eigenvalues. We use the LAPACK function dsyev_ to do so.  DSYEV computes all eigenvalues (first argument N) and, optionally, eigenvectors (first argument V) of a real symmetric matrix A. The second argument determine if we store the upper triangle A (U) of lower triangle (L). Third argument is the size of A.
         
void diagonalize_matrix(double* matrix, double* eigenvalues, int matrix_size) {
	double* work  = calloc(sizeof(double),64*matrix_size);
	int iffail = 0;
	do {
		int B2 = matrix_size, B3 = matrix_size;
		int B6 = 64*matrix_size, B8 = iffail;
		char B9 = 'N', B10 = 'L';
		dsyev_(&B9,&B10,&B2,matrix,&B3,eigenvalues,work,&B6,&B8);
		iffail = B8;
	} while(0);
	if(iffail != 0) printf("unexpected: diagonalisation failure ");
}


// ------- OTHER UTILITIES ------- //

//Compute the max between the two integers a and b
int intmax(int a, int b) {
	return (a>b) ? a : b;
}
//Compute the min between the two integers a and b
int intmin(int a, int b) {
	return (a<b) ? a : b;
}
//Not used in the following but good utility if you are interested into extracting the density matrix rho and save it to a file both normalized or not.
void outputrho(double* rho, int dim, int norm) {
	char buf[0x100];
	if (norm!=0) 
		snprintf(buf, sizeof(buf), "rhonormdim%d.dat", dim);
	else 
		snprintf(buf, sizeof(buf), "rhodim%d.dat", dim);
	
	FILE *fr = fopen(buf, "w");
	if (fr == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	for (int i1 =0; i1<dim; i1++) {
		for (int j1=0; j1<dim; j1++) {
			fprintf(fr, "%.17g\t", rho[dim*i1+j1]);
		}
		fprintf(fr, "\n");
	}
	fclose(fr);
}
	
//Not used in the following but good utility if you are interested into extracting the density matrix rho and print it in the terminal. Useful for debugging.
void printrho(double* rho, int dim) {
	printf("rho:\n");
	for (int i1 =0; i1<dim; i1++) {
		for (int j1=0; j1<dim; j1++) {
			printf("%f\t", rho[dim*i1+j1]);
		}
		printf("\n");
	}
}

// This function retrieves 9j from the array ar9j of already calculated 9j and compute the appropriate phase to recompose the 15j. This functions is specific for the conventions used for our 15j symbol.
double retrieve_9j(double* ar9j, int i1, int i2, int i3, int i4, int i5, int dim) {
	int sign = ((i1+i2+i3+i4+i5+2*dim-2)%2==0) ? 1 : -1;
	if (ar9j[i2*dim*dim*dim*dim+i3*dim*dim*dim+i1*dim*dim+i4*dim+i5]<DBL_MAX) return ar9j[i2*dim*dim*dim*dim+i3*dim*dim*dim+i1*dim*dim+i4*dim+i5];
	if (ar9j[i3*dim*dim*dim*dim+i2*dim*dim*dim+i1*dim*dim+i4*dim+i5]<DBL_MAX) return sign*ar9j[i3*dim*dim*dim*dim+i2*dim*dim*dim+i1*dim*dim+i4*dim+i5];
	if (ar9j[i2*dim*dim*dim*dim+i3*dim*dim*dim+i1*dim*dim+i5*dim+i4]<DBL_MAX) return sign*ar9j[i2*dim*dim*dim*dim+i3*dim*dim*dim+i1*dim*dim+i5*dim+i4];
	if (ar9j[i3*dim*dim*dim*dim+i2*dim*dim*dim+i1*dim*dim+i5*dim+i4]<DBL_MAX) return ar9j[i3*dim*dim*dim*dim+i2*dim*dim*dim+i1*dim*dim+i5*dim+i4];
	if (ar9j[i4*dim*dim*dim*dim+i5*dim*dim*dim+i1*dim*dim+i2*dim+i3]<DBL_MAX) return ar9j[i4*dim*dim*dim*dim+i5*dim*dim*dim+i1*dim*dim+i2*dim+i3];
	if (ar9j[i5*dim*dim*dim*dim+i4*dim*dim*dim+i1*dim*dim+i2*dim+i3]<DBL_MAX) return sign*ar9j[i5*dim*dim*dim*dim+i4*dim*dim*dim+i1*dim*dim+i2*dim+i3];
	if (ar9j[i4*dim*dim*dim*dim+i5*dim*dim*dim+i1*dim*dim+i3*dim+i2]<DBL_MAX) return sign*ar9j[i4*dim*dim*dim*dim+i5*dim*dim*dim+i1*dim*dim+i3*dim+i2];
	if (ar9j[i5*dim*dim*dim*dim+i4*dim*dim*dim+i1*dim*dim+i3*dim+i2]<DBL_MAX) return ar9j[i5*dim*dim*dim*dim+i4*dim*dim*dim+i1*dim*dim+i3*dim+i2];
	return DBL_MAX;
}



// -------   MAIN PART OF THE CODE     ------- //
// The code will compute the entanglement entropy (S) and the Renyi entropy of order 2 (R2) of the density matrix of a Bell Network state on a pentagram graph with all equal spins j where we trace over all the intertwiner space but one. We save the data in a file named "entanglement15j1node.dat" in the following format

// spin     entanglement entropy     Renyi entropy of order 2

int main()
{

          
        //The list of the spins we will compute
	double jlist[10] = {1,2,3,4,5,6,7,8,9,10};
	

        //Setup the output file and write the header
	FILE *f = fopen("entanglement15j1node.dat", "w"); // file to write output
	if (f == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	fprintf(f, "Spin \t S \t R2\n");


        //The computation is based on precalculated 6j and 9j symbols computed with fastwigxj. We refer to their documentation for the usage. Here we are using the tables with all the 6j and 9j with spins up to 25.
	fastwigxj_load("table_25.6j", 6, NULL);
	fastwigxj_load("hashed_25.9j", 9, NULL);
	wig_table_init(2*100,9);
	wig_temp_init(2*100);

	// Note that the arguments to wig3jj, wig6jj and wig9jj are 2*j and 2*m.  To be able to handle half-integer arguments.
	// for each spin j
	for (int ind=0; ind<10; ind++) {
		double j = jlist[ind];
		printf("Computing spin %f \n", j);
		//the five intertwiner spaces have dimension 2j +1
		int dim = (int)(2*j+1);

		//initialize matrix to store the density matrix
		double *rho;    
		rho = calloc(dim*dim, sizeof(double));
		if(rho == NULL) {
			fprintf(stderr, "malloc failed!\n");  
			return -1;   
		}
		//initialize matrix to store the 15j symbol
		double *ar15j;    
		ar15j = calloc(dim*dim*dim*dim*dim, sizeof(double));
		if(ar15j == NULL) {
			fprintf(stderr, "malloc failed!\n");   
			return -1;   
		}
		// initialize matrix to store the 9j symbols used to calculate the 15j symbols
		double *ar9j;    
		ar9j = calloc(dim*dim*dim*dim*dim, sizeof(double));
		if(ar9j == NULL) {
			fprintf(stderr, "malloc failed!\n");   
			return -1;   
		}
		for (int i1 = 0; i1<dim; i1++) 
			for (int i2 = 0; i2<dim; i2++) 
				for (int i3 = 0; i3<dim; i3++) 
					for (int i4 = 0; i4<dim; i4++) 
						for (int i5 = 0; i5<dim; i5++) 
							ar9j[i1*dim*dim*dim*dim+i2*dim*dim*dim+i3*dim*dim+i4*dim+i5] = DBL_MAX;
						
		//To perform the trace over the intertwiners we can choose any base. We are smart and we choose a basis where the 15j symbol is reduces into a product of 2 6j symbols and a 9j symbol. 
		double val6j1, val6j2, val9j;
		//phase
		int sgn;
		//dimension factors
		double prefac;				
		// the following loops calculate 9j and 15j and store them in the arrays
		for (int i1 =0; i1<dim; i1++) {
			for (int i2 =0; i2<dim; i2++) {
				for (int i3=abs(i1-i2); i3<=intmin(dim-1,i1+i2); i3++) {
					//the first 6j symbol
					val6j1 = fw6jja(2*i1, 2*i3, 2*i2, 2*j, 2*j, 2*j);
					for (int i4=0; i4<dim; i4++) {
						for (int i5=abs(i1-i4); i5<=intmin(dim-1, i1+i4); i5++) { 
							//phase
							sgn = 4*j + 2*i1 - (i2+i3+i4+i5);
							//dimension prefactor
							prefac = sqrt(2*i1+1)*sqrt(2*i2+1)*sqrt(2*i3+1)*sqrt(2*i4+1)*sqrt(2*i5+1);
							//second 6j symbol
							val6j2 = fw6jja(2*i1, 2*i4, 2*i5, 2*j, 2*j, 2*j);
							if (retrieve_9j(ar9j, i1, i2, i3, i4, i5, dim)!=DBL_MAX) val9j = retrieve_9j(ar9j, i1, i2, i3, i4, i5, dim);
							else {
								//9j symbol
								val9j = fw9jja( 2*i2, 2*i3,  2*i1, 2*j, 2*j, 2*i4, 2*j, 2*j, 2*i5);
								ar9j[i2*dim*dim*dim*dim+i3*dim*dim*dim+i1*dim*dim+i4*dim+i5] = val9j;
							}
								ar15j[i1*dim*dim*dim*dim+(i2*dim*dim*dim+i3*dim*dim+i4*dim+i5)] = sgn%2==0 ?  prefac*val6j1*val6j2*val9j : -prefac*val6j1*val6j2*val9j;
							}
					}
				}
			}
		}
		
		//To compute rho we just need to sum to trace the 15j symbol with itself on four intertwiner. Our Rho is not normalized to trrho=1 since the BN state we are using in this code is not normalized to 1.	
		tmmult(ar15j, ar15j, rho, dim, dim*dim*dim*dim, dim*dim*dim*dim, dim, dim*dim);
		double tr = trace(rho, dim);
		for (int i1 =0; i1<dim; i1++) {
			for (int j1=0; j1<dim; j1++) {
				rho[dim*i1+j1] = rho[dim*i1+j1]/tr;
			}
		}

		//We compute the eigenvalues \lambda_i of rho
		double* li = calloc(sizeof(double),dim);
		diagonalize_matrix(rho, li, dim);
		
		//Compute S
		// S(\rho) = \sum_i - \lambda_i \log \lambda_i //
		double S = 0;	
		for(int evec_i = 0; evec_i < dim; evec_i++) {
			S += -1.0*li[evec_i]*log(li[evec_i]);
		}
                
		//Compute R2
		// R2(\rho) = \sum_i - 2 \log \lambda_i //
		double R2 = 0;		
		for(int evec_i = 0; evec_i < dim; evec_i++) {
			R2 += li[evec_i]*li[evec_i];
		}
		R2 = - log(R2);                

		fprintf(f, "%f\t%.17g\t%.17g\n", j, S, R2);

		free(li);
		free(ar9j);
		free(ar15j);
		free(rho);
	}
	
	fclose(f);
	/* Remove tables from memory. */
	
	fastwigxj_unload(3);
	fastwigxj_unload(6);
	fastwigxj_unload(9);
	wig_temp_free();
	wig_table_free();

	return 0;
}